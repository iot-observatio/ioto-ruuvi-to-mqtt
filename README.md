# IoTo Ruuvi to MQTT

This project creates the MQTT Service for the IoTo architecture. The MQTT Service collects BLE messages sent by nearby RuuviTag sensors. It is tested on Raspberry Pi 4, but should work on a Linux machine supporting hcitool and Bluetooth LE. Please follow [the documentation](https://www.hackster.io/projects/5ea27e) for the background details to get an idea of the big picture.


## Project Prerequisites

At least one [RuuviTag](https://ruuvi.com) nearby your Raspberry Pi device. Also a MQTT broker need to be configured and available. Assuming a [Raspberry Pi OS](https://www.raspberrypi.org/software/) is in use, the following things need to be installed.

```
sudo apt install git python3-pip bluez bluez-hcidump
```

As we don't want to run python as root, let's loose up the requirements for hcitool.

```
sudo setcap 'cap_net_raw,cap_net_admin+eip' `which hcidump`
sudo setcap 'cap_net_raw,cap_net_admin+eip' `which hcitool`
```


## Setup

1. Clone the repo
    * I have used _joker_ as OS level user but default pi or whatever should fine
        * Using a different user is all about separating things. Just in case to limit damage when it hit the fan – kind of insurance
    * At `$HOME` I have created a directory `src/ioto` and cloned the repo there. So the project is located at `$HOME/src/ioto-ruuvi-to-mqtt (/home/joker/src/ioto-ruuvi-to-mqtt)` directory. Please feel free to create your own model
    * Just a reminder for me how I did it – just skip if it is too obvious for you:
    ```
    cd
    mkdir -p src/ioto
    cd src/ioto
    git clone https://gitlab.com/iot-observatio/ioto-ruuvi-to-mqtt.git
    cd ioto-ruuvi-to-mqtt
    ```


1. Install the python libs `make install`.
    ```
    make install
    ```


## Testing

To test interactively, populate correct values for the tags `<mqtt-user>` and `<mqtt-pass>` and enter
```
USER="<mqtt-user>" PASS="<mqtt-pass>" CLIENTID=$(hostname) SERVER=localhost make test
```


## Running as a service
To run as a service and allow autostart on reboot, first we need to install `supervisor`
```
sudo apt install supervisor
```

Then copy the file `ioto-ruuvi-to-mqtt-supervisord.sample` as `/etc/supervisor/conf.d/ioto-ruuvi-to-mqtt.conf`. Edit the conf file and modify values of the keys:
- [ ] key `directory=` to value of your `ioto-ruuvi-to-mqtt` directory (mine: `directory=/home/joker/src/ioto/ioto-ruuvi-to-mqtt`)
- [ ] key `environment=` to value of your MQTT environment (mine: `environment=SERVER="localhost",CLIENTID="jrpi01",USER="iuser",PASS="LosLocos#",INTERVAL="60"`)
- [ ] key `user=` to value of your OS user if not default pi (mine: `user=joker`)


Service `supervisord` need to be restarted:
```
sudo service supervisor restart
```
You can confirm after that `ioto-ruuvi-to-mqtt` started by looking at:
```
sudo service supervisor status
```

The MQTT broker should start receiving messages in a minute or so.


## All things done?

That's all for the project setup. By now the data from your RuuviTag's should be available in `My Cloud Platform`. Enjoy!

