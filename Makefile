.PHONY: install
install:
	python3 -m pip install -r requirements.txt

.PHONY: test
test:
	python3 ioto-ruuvi-getpub.py

