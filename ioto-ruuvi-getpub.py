# The Eclipse Distribution License is available at
#   http://www.eclipse.org/org/documents/edl-v10.php.
#
# Contributors:
#   Jouko Loikkanen - initial implementation
# Copyright (c) 2021 Jouko Loikkanen
# All rights reserved.

import asyncio
import json
import os
import sys
import time
import paho.mqtt.client as mqtt
from ruuvitag_sensor.ruuvi import RuuviTagSensor
from concurrent.futures import ProcessPoolExecutor
from multiprocessing import Manager
from datetime import datetime

# Get environment variables
mqtt_server = os.getenv("SERVER")
mqtt_user = os.getenv("USER")
mqtt_pass = os.getenv("PASS")
mqtt_clientid = os.getenv("CLIENTID", "")
ble_scan_interval = int(os.getenv("INTERVAL", 30))

# MQTT topic
def get_topic(mac):
    return "IoT/ruuvi/{}".format(mac.replace(':', '').lower())

# Publish to the MQTT Service
async def handle_queue(queue):
    while True:
        if not queue.empty():
            # Let's use only the latest data for each RuuviTag sensor
            data = {}
            while not queue.empty():
                (mac, value) = queue.get()
                data[mac] = value
            for key, value in data.items():
                value['date'] = datetime.now().isoformat()
                print(key + ":" + json.dumps(value))
                mqtt_c.publish(key, json.dumps(value))
        time.sleep(ble_scan_interval)

# Get data from RuuviTag's
def run_get_datas_background(queue):
    def handle_data(data):
        mac = get_topic(data[0])
        value = data[1]
        queue.put((mac, value))

    RuuviTagSensor.get_datas(handle_data)

# Handle MQTT Service disconnection; stop the world if disconnected
def on_disconnect(client, userdata, rc):
    print("ERROR: on_disconnect: Please check state of MQTT Service and network: %s" %(mqtt.error_string(rc)))
    os._exit(rc)

# Handle MQTT Service connection; stop the world if connection fail
def on_connect(mqtt_c, userdata, flags, rc):
    print("INFO: on_connect: %s" %(mqtt.error_string(rc)))
    if rc != 0:
        print("ERROR: on_connect: Please check connection options and network state: %s" %(mqtt.error_string(rc)))
        os._exit(rc)


# Exile On Main St
if __name__ == "__main__":
    if mqtt_server is None or mqtt_user is None or mqtt_pass is None:
        exit('ERROR: Please set environment variables SERVER, USER and PASS')

    mqtt_c = mqtt.Client(mqtt_clientid)
    mqtt_c.username_pw_set(mqtt_user, mqtt_pass)
    mqtt_c.on_disconnect = on_disconnect
    mqtt_c.on_connect = on_connect
    mqtt_c.loop_start()
    try:
        mqtt_c.connect(mqtt_server)
        while not mqtt_c.is_connected():
            print("INFO: connecting ...")
            time.sleep(1)
    except Exception as e:
        exit(e)

    try:
        manager = Manager()
        queue = manager.Queue()
        executor = ProcessPoolExecutor()
        executor.submit(run_get_datas_background, queue)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(handle_queue(queue))
    except Exception as e:
        exit(e)
